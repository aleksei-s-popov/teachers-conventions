# PHP style conventions and automated CS validation

PHP Code Sniffer configuration and git hooks for automated CS check used by Teachers Team

## common
https://editorconfig.org/


## php

### links

* https://www.php-fig.org/psr/
* https://github.com/squizlabs/PHP_CodeSniffer
* https://github.com/slevomat/coding-standard

### Installation

Add package to `repositories` block in composer.json 

```json
{
    "type": "git",
    "url": "git@bitbucket.org:aleksei-s-popov/teachers-conventions.git"
}
```

Install the package
```bash
composer require --dev aleksei-s-popov/teachers-conventions
````

Add scripts configuration to composer.json file.

```json
"scripts": {
    "post-install-cmd": [
      "@copy-editor-config",
      "@install-pre-commit-hook"
    ],
    "post-update-cmd": [
      "@copy-editor-config",
      "@install-pre-commit-hook"
    ],
    "copy-editor-config": "TeachersConventions\\Composer\\ScriptHandler::copyEditorConfig",
    "test-pre-commit-hook": "TeachersConventions\\Composer\\ScriptHandler::testGitPreCommitHook",
    "install-pre-commit-hook": "TeachersConventions\\Composer\\ScriptHandler::installGitPreCommitHook",
    "remove-pre-commit-hook": "rm .git/hooks/pre-commit"
  },
  "extra": {
    "teacher-conventions": {
      "PHP_BIN": "php",
      "PHPCS_BIN": "vendor/bin/phpcs",
      "PHPCS_CODING_STANDARD": "vendor/skyeng/teachers-conventions/php/phpcs.xml",
      "PHPCS_FILE_PATTERN": "optional filename mask to check by sniffer in egrep format (defoult is \\.php$)",
      "PHPCS_IGNORE_PATTERN": "optional filename mask in egrep format for files to skip"
    }
  }
```

### Usage

Run check on files, that a ready for commit:
```bash
git add CHANGED_FILE.php

composer run-script test-pre-commit-hook
```
if there are style errors in `CHANGED_FILE.php`, sniffer will list the violations.

Variables listed in composer.json, could be overridden:

```bash
composer run-script test-pre-commit-hook -- --PHP_BIN=custom_php_path --PHPCS_BIN=custom_phpcs_path --PHPCS_CODING_STANDARD=PSR1
```

There is a way to install pre-commit hook and run sniffer on before each commit:

```bash
composer run-script install-pre-commit-hook
```
With this pre-commit hook every file in commit must fit into code style convention,
or commit will be aborted.

## typescript

https://palantir.github.io/tslint/


## author
[Rishat Girfanov](https://github.com/xbg)

Based on https://github.com/s0enke/git-hooks/blob/master/phpcs-pre-commit/pre-commit
