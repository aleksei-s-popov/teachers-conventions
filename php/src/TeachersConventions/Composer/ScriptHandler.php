<?php

namespace TeachersConventions\Composer;

use Composer\Script\Event;

class ScriptHandler
{
    public static function copyEditorConfig(Event $event): void
    {
        if (!$event->isDevMode()) {
            return;
        }

        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');

        $dstPath = dirname($vendorDir) . '/.editorconfig';
        $srcPath = $vendorDir . '/aleksei-s-popov/teachers-conventions/common/.editorconfig';

        copy($srcPath, $dstPath);
    }

    public static function installGitPreCommitHook(Event $event): void
    {
        if (!$event->isDevMode()) {
            return;
        }

        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        $dstPath = \dirname($vendorDir) . '/.git/hooks';

        if (!file_exists($dstPath)) {
            return;
        }

        self::savePreCommitHookScript($dstPath . '/pre-commit', self::preparePreCommitHookOptions($event));
    }

    private static function savePreCommitHookScript(string $dstPath, array $options): void
    {
        $srcPath = $options['SRC_PATH'];

        $template = \file_get_contents($srcPath);

        if ($template === false) {
            throw new \RuntimeException(\sprintf('Can not read pre-commit hook template file "%s"', $srcPath));
        }

        foreach ($options as $key => $value) {
            $template = \str_replace('%' . $key . '%', $value, $template);
        }

        \file_put_contents($dstPath, $template);
        \chmod($dstPath, 0755);
    }

    private static function preparePreCommitHookOptions(Event $event): array
    {
        $extras = $event->getComposer()->getPackage()->getExtra();

        $inputArgs = self::parseArguments($event);
        $options = \array_merge([
            'PHP_BIN'               => 'php',
            'PHPCS_BIN'             => 'bin/phpcs',
            'PHPCS_CODING_STANDARD' => 'PSR2',
            'PHPCS_FILE_PATTERN'    => '\.php$',
            'PHPCS_IGNORE_PATTERN'  => '',
        ], $extras['teacher-conventions'] ?? [], $inputArgs);

        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        $srcPath = $vendorDir . '/aleksei-s-popov/teachers-conventions/php/pre-commit.dist';
        $options['SRC_PATH'] = $srcPath;

        return $options;
    }

    private static function parseArguments(Event $event): array
    {
        $inputArgs = [];
        foreach ($event->getArguments() as $argument) {
            if (\preg_match('/^--([^=]+)=(.*)/', $argument, $match)) {
                $inputArgs[$match[1]] = $match[2];
            }
        }

        return $inputArgs;
    }

    public static function testGitPreCommitHook(Event $event): void
    {
        if (!$event->isDevMode()) {
            return;
        }

        $dstPath = tempnam(\sys_get_temp_dir(), 'pre-commit-hook-test');
        self::savePreCommitHookScript($dstPath, self::preparePreCommitHookOptions($event));
        \passthru("bash $dstPath");
    }
}
